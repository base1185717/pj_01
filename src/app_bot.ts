import { UpdateDriverI } from "./bot_services/update_driver"
import { myMiddleware } from "./bot_services/middleware/middleware_service";

export class AppBot {
    private updateDriver: UpdateDriverI

    setUpdateDriver(updateDriver: UpdateDriverI) {
        this.updateDriver = updateDriver;
    }

    useMiddleware(handler){
        myMiddleware.use(handler)
    }

    async prepare() {
        //await Connection.getConnection();
    }

    public async run() {
        try {
            await this.prepare();
            this.updateDriver.getUpdates();
        } catch (err) {
            console.error(err)
        }
    }   
}