require('dotenv').config()
let https = require(`https`)

export class OutgoingMessageService{

    static async request_messages(update_id):Promise<string> {
        const TELEGRAM_URI = `https://api.telegram.org/bot${process.env.BOT_TOKEN}/getUpdates?${update_id ? `offset=${update_id}` : ''}&timeout=30`;
        return new Promise((resolve, reject) => {
            const req = https.get(TELEGRAM_URI, async (res) => {
                console.log('statusCode:', res.statusCode);
                res.setEncoding('utf8');
                let response_data = '';
                res.on('data', (d) => {
                    //process.stdout.write(d);
                    response_data += d;
                });

                res.on('end', () => {
                    resolve(response_data)
                })

                res.on('error', (err) => {
                    reject(err);
                })

            });

            req.on('error', (e) => {
                reject(e);
            });
        })
    } 
    
    static async sendMessage(text_message, chat_id) {

        new Promise((resolve, reject) => {
            const postData = JSON.stringify(
                {
                    chat_id: chat_id,
                    text: text_message
                }
            );

            const options = {
                hostname: 'api.telegram.org',
                port: 443,
                path: `/bot${process.env.BOT_TOKEN}/sendMessage`,
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                    'Content-Length': Buffer.byteLength(postData)
                }
            };

            const req = https.request(options, (res) => {
                res.setEncoding('utf8');
                let res_data = '';
                res.on('data', (d) => {
                    res_data += d;
                });
                res.on('end', () => {
                    resolve(res_data);
                });
                res.on('error', (err) => {
                    reject(err)
                })
            })

            req.on('error', (err) => {
                reject(err);
            });

            req.write(postData);

            req.end();
        })

    }

}