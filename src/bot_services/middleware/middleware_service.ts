import { Incoming_MessageT } from "../message_handler";

export type Middleware<T> = (context: T, next: Next<T>) => Promise<void> | void;
export type Next<T> = (context) => void | Promise<T|void>;

class MyMiddleware <T> {

    middlewares: Array<Middleware<T>>;
    //middlewares: Middleware<T>[];

    constructor() {
        this.middlewares = [];
    }

    use(mw:Middleware<T>):void{
        this.middlewares.push(mw)
    }

    async dispatch(context:T): Promise<T|void> {
        return this.runMiddlewares(context, this.middlewares)
    }

    async runMiddlewares(context:T, middlewares : Middleware<T>[]):Promise<T|void> {
        if(!middlewares.length) return context;
        const currentMW = middlewares[0];
        return currentMW(context, async (context):Promise<T|void> =>{
            return this.runMiddlewares(context, middlewares.slice(1))
        })
    }
}

const myMiddleware =  new MyMiddleware<Incoming_MessageT>()

export { myMiddleware }