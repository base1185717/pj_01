
export abstract class HandlersFactoryBase {
    

    abstract resolveDependencies()

    getInstanse(entity) {
        //общие зависимотси
        const dependencies = this.resolveDependencies();
        // + дополнительные
        return new entity(...dependencies)
    }
}