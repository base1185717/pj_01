import { Incoming_MessageT } from "../message_handler";
import { Next } from "../middleware/middleware_service";

export interface ResponseT{
    message: string;
}

export interface CommandHandlerI {
    handle(message:Incoming_MessageT, next: Next<Incoming_MessageT>):Promise<ResponseT>
}

