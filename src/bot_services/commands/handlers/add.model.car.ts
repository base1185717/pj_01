import { CommandHandlerI } from "../command.handler.interface";
import { ResponseT } from "../command.handler.interface";
import { Incoming_MessageT } from "../../message_handler";
import { CommandsEnum, UserCommandEntity } from "./../../../common_service/db_layer/models/user.command.entity";
import { PostgresRepositoryFactory, UserInMemoryRepositoryFactory } from "../../../common_service/db_layer/factories/repository_factories/common.repository.factory";
import { TaskEntity } from "../../../common_service/db_layer/models/task_entity";
import { TaskStatus } from "../../../common_service/db_layer/models/task_entity";

export class AddModelCarHandler implements CommandHandlerI{


    async handle(message:Incoming_MessageT):Promise<ResponseT>{
        const model = message.text.toLowerCase().trim();
        const models = ['focus', 'm3', 'lx200'];
        const userCommandRepository = UserInMemoryRepositoryFactory.getInstanse();
        const currentCommandUser = <UserCommandEntity> await  userCommandRepository.find(message.from.id);
        //репозиторий базы данных
        if(models.indexOf(model) == -1 ){
            currentCommandUser.expectedСommand = CommandsEnum.Model;
            await userCommandRepository.saveEntity(currentCommandUser);
            return {message:"Такая модель автомобиля отсутствует. Внесите повторно:"}
        }
        const json = JSON.parse(currentCommandUser.value);
        json.model = model;
        currentCommandUser.value  = JSON.stringify(json);
        await userCommandRepository.saveEntity(currentCommandUser);
        const repository = new PostgresRepositoryFactory(TaskEntity).getInstanse();
        const tentity = new TaskEntity();
        tentity.task_status = TaskStatus.Active;
        await repository.saveEntity(tentity);
        return {message:"Модель авто запомнена. Задача добавлена в работу. Ожидайте ответ"}
    } 

}