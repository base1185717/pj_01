import { CommandHandlerI } from "../command.handler.interface";
import { ResponseT } from "../command.handler.interface";
import { Incoming_MessageT } from "../../message_handler";
import { UserInMemoryRepositoryFactory } from "../../..//common_service/db_layer/factories/repository_factories/common.repository.factory";
import { CommandsEnum, UserCommandEntity } from "./../../../common_service/db_layer/models/user.command.entity";

export class AddMarkCarHandler implements CommandHandlerI{


    async handle(message:Incoming_MessageT):Promise<ResponseT>{
        const mark = message.text.toLowerCase().trim();
        const marks = ['bmw', 'lexus', 'ford'];
        const userCommandRepository = UserInMemoryRepositoryFactory.getInstanse();
        const currentCommandUser = <UserCommandEntity> await  userCommandRepository.find(message.from.id);
        if(marks.indexOf(mark) == -1 ){
            currentCommandUser.expectedСommand = CommandsEnum.Mark;
            await userCommandRepository.saveEntity(currentCommandUser);
            return {message:"Такая марка автомобиля отсутствует. Выберите повторно:"}
        }
        currentCommandUser.value =  JSON.stringify({mark:mark});
        await userCommandRepository.saveEntity(currentCommandUser); 
        return {message:"Марка авто запомнена. Введите название модели автомобиля:"}
    } 

}