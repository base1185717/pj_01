import { CommandHandlerI } from "../command.handler.interface";
import { ResponseT } from "../command.handler.interface";
import { Incoming_MessageT } from "../../message_handler";

//удалить задачу/задачи
export class ClearCommandHandler implements CommandHandlerI{

    async handle(message:Incoming_MessageT):Promise<ResponseT>{

        return {message:"команда удалена"}
    } 

}