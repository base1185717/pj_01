import { CommandHandlerI } from "../command.handler.interface";
import { ResponseT } from "../command.handler.interface";
import { Incoming_MessageT } from "../../message_handler";
import { HandlersFactoryBase } from "../handlers.factory";
import { UserCommandEntity } from "./../../../common_service/db_layer/models/user.command.entity";
import { PostgresRepositoryFactory, UserInMemoryRepositoryFactory } from "./../../../common_service/db_layer/factories/repository_factories/common.repository.factory";
import { ClientModel } from "./../../../common_service/db_layer/models/client_entity";
import { TaskEntity, TaskStatus } from "./../../../common_service/db_layer/models/task_entity";

export class AddCarHandler implements CommandHandlerI {

    async handle(message: Incoming_MessageT): Promise<ResponseT> {
        const repository = new PostgresRepositoryFactory(TaskEntity).getInstanse();

        const result = await repository.find({ where: { id: 1 /*message.from.id*/ } });

        let entity = new TaskEntity
        entity.id= 10;
        entity.task_status = TaskStatus.Active;
        entity.time_last_check = new Date;
        console.log(`entity`)
        console.log(entity)

        await repository.updateEntity(entity)
        const userCommandRepository = UserInMemoryRepositoryFactory.getInstanse();
        const currentCommandUser = <UserCommandEntity>await userCommandRepository.find( /* { where: {id : message.from.id} }*/ message.from.id);
        await userCommandRepository.saveEntity(currentCommandUser)
        return { message: "введите название марки автомобиля:" }
    }

}

