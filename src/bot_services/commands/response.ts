import { ResponseT } from "./command.handler.interface";
import { Incoming_MessageT } from "../message_handler";

class Response implements ResponseT{
    message: string;
}