import { CommandsEnum } from "../../common_service/db_layer/models/user.command.entity";
import { AddCarHandler } from "./handlers/add.car.handler";
import { AddMarkCarHandler } from "./handlers/add.mark.car.handler";
import { AddModelCarHandler } from "./handlers/add.model.car";

// to do изменить добавление команд
class CommandRegistry{

    private commands: Map<string, any > = new Map;

    use(command:CommandsEnum, handler:any){
        this.commands.set(command, handler);
    }

    resolve(name){
        if(this.commands.has(name))return this.commands.get(name);
    }    
}

const commandRegistry = new CommandRegistry

commandRegistry.use(CommandsEnum.AddCar, new AddCarHandler); 

commandRegistry.use(CommandsEnum.Mark, new AddMarkCarHandler);

commandRegistry.use(CommandsEnum.Model, new AddModelCarHandler);


export {commandRegistry};