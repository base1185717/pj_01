import { MessageHandler } from "./message_handler";
import { OutgoingMessageService } from "./outgoing_messages_service/outgoingmessage";

export interface UpdateDriverI{
    getUpdates():void
}

export class LongPoolingUpdateDriver implements UpdateDriverI  {

    async getUpdates() {
        let update_id_1 = null;
        while (true) {
            const r = await OutgoingMessageService.request_messages(update_id_1);
            const z = JSON.parse(r);
            const result = z.result;
            if (result.length != 0) {
                //const { message } = result;
                result.forEach(async (item) => {
                    const messageHandler = new MessageHandler(item)
                    await messageHandler.handle(/*new MessageParser*/);
                });
                const { update_id } = result[result.length - 1];
                console.log(`айди последнего апдейта ` + update_id);
                update_id_1 = update_id + 1;
            }
        }
    }
}