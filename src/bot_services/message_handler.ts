import { OutgoingMessageService } from "./outgoing_messages_service/outgoingmessage";
import { CommandNameResolver } from "./command.resolver";
import { commandRegistry } from "./commands/registry.command.handlers";
import { UserInMemoryRepositoryFactory } from "../common_service/db_layer/factories/repository_factories/common.repository.factory";
import { myMiddleware } from "./middleware/middleware_service";

export type Incoming_MessageT = {
  message_id: number,
  from: {
    id: number, //587955832,
    is_bot: boolean,
    first_name: string,
    username: string,
    language_code: string //'ru'
  },
  chat: {
    id: number,
    first_name: string,
    username: string,
    type: string //'private'
  },
  date: number //1679652963,
  text: string //'Текст'

}

export class MessageHandler {
  
  constructor( private message) {}

  async handle(/*messageParser: MessageParser*/) {
    const { message } = this.message;
    try {
      const userCommandRepository = UserInMemoryRepositoryFactory.getInstanse();
      const commandNameResolver = new CommandNameResolver(userCommandRepository)
      const commandName = await commandNameResolver.resolve(message)
      const command = commandRegistry.resolve(commandName)
      const result = await myMiddleware.dispatch(message)
      const resultMessage = command ? await command.handle(result) : { message: 'эхо' };
      await OutgoingMessageService.sendMessage(`Эхо - ${resultMessage.message}`, message.chat.id)
    } catch (err) {
      console.log(err)
      await OutgoingMessageService.sendMessage(`Эхо - ошибка обработчкиа}`, message.chat.id)
    }
  }
}