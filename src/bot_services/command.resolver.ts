import { typeCommandChainEnum } from "../common_service/db_layer/save_layer/inmemory/user.command.registry";
import { Incoming_MessageT } from "./message_handler";
import { UserCommandEntity } from "../common_service/db_layer/models/user.command.entity"; 
import { CommandsEnum } from "../common_service/db_layer/models/user.command.entity";
import { BaseDataMapper } from "../common_service/db_layer/mappers/base.mapper";
import { BaseRepository } from "../common_service/db_layer/repositories/base.repository";

const CommandChains = {
    AddCarChain: [
        CommandsEnum.AddCar,
        CommandsEnum.Mark,
        CommandsEnum.Model
    ],
}

const StartChainCommand = [
    [CommandsEnum.AddCar, typeCommandChainEnum.AddCar]

]

class CommandNameParser {
    constructor(private incomingMessage) { }

    isCommand() { 
        return this.incomingMessage.text.slice(0, 1) === '/' ? true : false;
    }

    isTimedOut(currentCommand): boolean {
        return (this.incomingMessage.date - currentCommand.timestamp > 30 * 1000) ? true : false;
    }

    isStartChainCommand() {
        return StartChainCommand.find((item) => item[0] === this.incomingMessage.text) ? true : false
    }

}

class CommandNameResolver /* <Repository extends UserCommandRepository<UserCommandEntity,InMemoryDataMapper<UserCommandEntity>>> */ {
    
    constructor(private userCommandRepository:  BaseRepository<UserCommandEntity,BaseDataMapper<UserCommandEntity>> ) {}

    async resolve(incoming_message: Incoming_MessageT) {

        const id = incoming_message.from.id
        const currentCommandUser = <UserCommandEntity> await  this.userCommandRepository.find(id);
        const commandParser = new CommandNameParser(incoming_message);
        let currentCommandN:string|undefined;
        if (currentCommandUser) {
            if (commandParser.isTimedOut(currentCommandUser)) { 
                //to do если ожидание новой команды в цепочке слишком велико
                //this.executedCommands.delete(id);
                return; 
            }
            if (commandParser.isCommand()) {
                // to do ... 
            } else/* пришло значение, но не команда */ {
                const chaintype = currentCommandUser.typeCommandChain;
                const chain = CommandChains[chaintype];
                const expectedСommand = currentCommandUser.expectedСommand;
                const indx = chain.indexOf(expectedСommand);
                currentCommandUser.expectedСommand = indx === chain.length - 1 ? chain[chain.length - 1] : chain[indx + 1]
                currentCommandUser.timestamp = Date.now();
                await this.userCommandRepository.saveEntity(currentCommandUser)
                currentCommandN = expectedСommand
            }
        }
        if (commandParser.isStartChainCommand()) {
            const startCommand = incoming_message.text;
            const [command, chaintype] = StartChainCommand.find((item) => item[0] === startCommand);
            const chain = CommandChains[chaintype]
            currentCommandN = command;
            this.userCommandRepository.saveEntity({
                userId:id,
                expectedСommand: chain[1],
                typeCommandChain : <typeCommandChainEnum> chaintype,
                timestamp: Date.now()
            });
        }
        return currentCommandN;
    }

}

export { CommandNameResolver };





