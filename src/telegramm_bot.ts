import { AppBot } from "./app_bot";
import { Next } from "./bot_services/middleware/middleware_service";
import { LongPoolingUpdateDriver } from "./bot_services/update_driver";
import { Incoming_MessageT } from "./bot_services/message_handler";

const bot = new AppBot;
bot.setUpdateDriver(new LongPoolingUpdateDriver());

bot.useMiddleware((context: Incoming_MessageT, next: Next<Incoming_MessageT>) => {
    return next(context);
});

bot.useMiddleware((context: Incoming_MessageT, next: Next<Incoming_MessageT>) => {
    return next(context);
});


bot.run();




