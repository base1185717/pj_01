import { UserCommandEntity } from "../../models/user.command.entity";
import { BaseDataMapper } from "../../mappers/base.mapper";
import { userCommandRegistryObject } from "../../save_layer/inmemory/user.command.registry";
import { BaseRepository } from "../../repositories/base.repository";
import { PostgresDataMapper } from "../../mappers/postgres.data.mapper";
import { PostgresStoreLayer } from "../../save_layer/postgres/postgres_store_layer";
import { PostgresQueryBuilder } from "../../save_layer/postgres/query_builder";
import { TaskRepository } from "../../repositories/task.repository";
import { EntityI, TaskEntity } from "../../models/task_entity";


export class CommonRepositoryFactory {

    static getInstanse = () => {
        const mapper = new BaseDataMapper<UserCommandEntity>(userCommandRegistryObject)
        const entity = new UserCommandEntity;
        const repository = new BaseRepository<UserCommandEntity, typeof mapper>( mapper  )
        return repository;
    }
}

export class UserInMemoryRepositoryFactory {

    static getInstanse = () => {
        const mapper = new BaseDataMapper<UserCommandEntity>(userCommandRegistryObject);
        const entity = new UserCommandEntity;
        const repository = new BaseRepository<UserCommandEntity, typeof mapper>(mapper )
        return repository;
    }
}

export class PostgresRepositoryFactory<T extends EntityI >{
 
    constructor(private model : {new(): T }){}

    getInstanse = () =>{
        const store_layer = new PostgresStoreLayer<T>(PostgresQueryBuilder);
        const model = new this.model;
        const mapper =  new PostgresDataMapper<T>(store_layer, model.table)
        const repository = new BaseRepository<T, typeof mapper >(mapper  );
        return repository;
    }
}



