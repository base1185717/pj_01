import { TaskEntity } from "../models/task_entity";
import { BaseRepository } from "./base.repository";
import { BaseDataMapper } from "../mappers/base.mapper";


export class TaskRepository<P, MapperType extends BaseDataMapper<P>>  extends BaseRepository<P, MapperType>  {

    createEntity(): P | void {
        this.dataMapper.find(1)
    }

    /*async getEntity(id): Promise<P> {
        return <P>{ df: 'sd' }
    }*/

    getAll(): Promise<P[]> {
        return this.dataMapper.getAll()
    }

}