import { DataMapperI ,BaseDataMapper } from "../mappers/base.mapper";

export /*abstract*/ class BaseRepository<P, MapperType extends BaseDataMapper<P>>{

    //constructor( protected dataMapper: BaseDataMapper<P> , protected model:{new(): P}    ) {}

    constructor(protected dataMapper:  MapperType ,/*  table: string  {new(): P}*/  ) {
    }

    async find(id:any):Promise<P | void>{
        return this.dataMapper.find(id)
    }
    getAll(){
        return this.dataMapper.getAll()
    }

    createEntity() { };

    async saveEntity<T extends P>(entity: T): Promise<T | void> {
        await this.dataMapper.save(entity);
    };

    async updateEntity(entity) { 
        await this.dataMapper.edit(entity)
    };

    deleteEntity() { };
}