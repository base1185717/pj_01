import { EntityI } from "../models/task_entity";
import { StoreLayerI } from "../save_layer/save.layer.base.interface";
import { BaseDataMapper, DataMapperI } from "./base.mapper";

type FindOptions = {
    where?: object
    orderBy?: { field: string, direction: 'ASC' | 'DESC' }
}

enum FindOptionsEnum {
    Where = 'where',
    OrderBy = 'orderBy'
}

const findOptions = {
    where: {},
    orderBy: {}
}

export class PostgresDataMapper<T extends EntityI> extends BaseDataMapper<T> /*implements DataMapperI*/ {
  
    constructor(protected store: StoreLayerI<T>, private table:string) {
        super(store)
    }

    async find(params) {
        params.table = this.table;
        const data = await this.store.get(params);
        if (!data.rowCount) {
            return null
        }
        return data.rows;
    }

    async save<T extends EntityI>(entity: T) {
        const { table, ...props } = entity;
        const mapped = {
            table: table,
            params: Object.entries(props)
        }
        await this.store.set(mapped);
    }

    async edit(entity: any): Promise<void> {
        return this.store.edit(entity)
    }

    async getAll(): Promise<T[]> {
        return this.store.getAll()
    }

}

