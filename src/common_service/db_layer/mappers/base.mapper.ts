import { EntityI } from "../models/task_entity"
import { StoreLayerI } from "../save_layer/save.layer.base.interface"

export interface DataMapperI {

    save<T extends EntityI>(entity: T): Promise<void>

    get(id): Promise<any | void> | any

}


export class BaseDataMapper<P> {

    constructor( /*protected model:{new(): P}, */ protected store: StoreLayerI<P>) { }

    async save(entity) {
        return this.store.set(entity);
    }

    async find(/* id?:number,*/ params?:any):Promise<P|null|void> {
        return this.store.get(params);
    }

    async edit(entity:P){
        return this.store.edit(entity)
    }

    async set(entity:P): Promise<P|void>{
        return this.store.set(entity);
    }

    async getAll():Promise<P[]>{
        return this.store.getAll()
    }

    async delete(){
        
    }
    
}