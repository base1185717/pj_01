export enum CommandsEnum {
    AddCar = '/addcar',
    Mark = 'mark',
    Model = 'model'
}

enum typeCommandChainEnum {
    AddCar = 'AddCarChain'
}

export class UserCommandEntity {
    userId?: number
    expectedСommand: CommandsEnum
    typeCommandChain: typeCommandChainEnum
    timestamp: number
    value?: string
}