
export enum TaskStatus{
    Active ='active',
    Stopped = 'stopped'
}

export interface EntityI{
    table?:string
    id?:number
}

/*CREATE TABLE tasks (
    id int primary key,
    task_status varchar(255),
    time_last_check timestamp,
)*/;

//to do -  дополнить декораторами
export class TaskEntity implements EntityI{

    table?:string = 'tasks';

    id? : number;

    //client_id : number;

    task_status : TaskStatus;

    //время последней проверки задачи
    time_last_check : Date;

}


