
export interface StoreLayerI<P>{

    get(id):Promise<P>|P|void|any

    set(entity)

    getAll():Array<any>

    edit(entity:P)

}