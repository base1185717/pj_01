import { StoreLayerI } from "../save.layer.base.interface";
import { PostgresQueryBuilder } from "./query_builder";
import { connection } from "./connection";

type mappedType = {
    table:string,
    params: Array<[string, string]>
}

export class PostgresStoreLayer <P> implements StoreLayerI <P> {

    constructor( /* private model: {new(): P}, */ private queryBuilder: { new() : PostgresQueryBuilder<P> } ) {}

    async get(mapped){
        let sql = ''
        if('where' in mapped){
            sql = (new this.queryBuilder).select(mapped.table).where(mapped['where'] ).getSql();
        }
        const client = connection.getConnection();
        const res = await client.query(sql);
        return res
    }

    async set(mapped){
        const client = connection.getConnection();
        const sql = (new this.queryBuilder).insert(mapped.table).values(mapped.params).getSql();
        return client.query(sql)
    }

    getAll(){
      return []  //this.queryBuilder 
    }

    async edit(entity){
        const sql = (new this.queryBuilder).update(entity.table).set(entity).getSql();
        console.log(`sql`)
        console.log(sql)
    }

    async insert(mapped) {

        const client = connection.getConnection();
        /**
         * ошибка Client has already been connected. You cannot reuse a client.
         * тут некоторые непонятки относительно соединения с базой.
         * пишут что нельзя переиспользовать отдельно созданный объект client (???) 
         * и что лучше создавать новый отдельный
         */
        const sql = (new this.queryBuilder).insert(mapped.table).values(mapped.params).getSql();
        return client.query(sql)
    }

}