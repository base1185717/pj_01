require('dotenv').config()
const { Client, Pool } = require('pg')

/*const client = new Client({
    host: process.env.PG_HOST,
    port: process.env.PG_PORT,
    user: process.env.PG_USER,
    password: process.env.PG_PASSWORD,
})*/

console.log(`pool connect`)
class Connection {

    private pool;

    constructor() {
        this.pool = new Pool({
            host: process.env.PG_HOST,
            port: process.env.PG_PORT,
            user: process.env.PG_USER,
            password: process.env.PG_PASSWORD,
            max: 20,
            idleTimeoutMillis: 30000,
            connectionTimeoutMillis: 2000,
        })
    }

    getConnection() {
        return this.pool
    }
}

const connection = new Connection

export {connection};