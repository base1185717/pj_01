import { EntityI } from "../../models/task_entity";

export class PostgresQueryBuilder<T extends EntityI> {

    private sql = '';

    private table =''

    insert(tableName: string) {
        this.table = tableName;
        this.sql += `INSERT INTO "${tableName}" `;
        return this;
    }

    update(tableName: string){
        this.table = tableName;
        this.sql += `INSERT INTO "${tableName}" `;
        return this;
    }

    set(entity:T){
        type entityKeys = keyof typeof entity; 
        let propId = <entityKeys> Object.keys(entity).find(item => item === 'id')
        let str = `SET `;
        if (propId) {
            const {id, ...other} = entity;
            const setParams =  Object.entries(other)
            setParams.forEach( (item, indx )=>{
                const [key, value] = item;
                let v = value;
                if( value instanceof Date ){
                    v = value.toISOString();
                }
                str += `"${key}" = '${v}'${ !(indx ===  setParams.length - 1) ? ',' : '' }`;
            })
            this.sql += str;
            this.where({id: id})
            
        }else{
            // ...
        }
        return this;
    }

    values(params: Array<[string, string]>) {
        let column_str = '';
        let value_str = '';
        params.forEach((element: [string, string], indx) => {
            let [key, value] = element;
            column_str += `${indx == 0 ? '(' : ''}${key}${indx == params.length - 1 ? ') ' : ', '}`;
            value_str += `${indx == 0 ? ' (' : ' '}'${value}'${indx == params.length - 1 ? '); ' : ', '}`;
        });
        this.sql += `${column_str} VALUES ${value_str}`;
        return this;
    }

    select(tableName: string) {
        this.sql += `SELECT * FROM "${tableName}"`;
        this.table = tableName;
        return this;
    }

    where(param /* { id: 1 }*/, template = null/** "user.id = :id"   */){
        let newstr = '';
        Object.keys(param).forEach((item, indx) => {
            const y = template ? template : `"${this.table}"."${item}" = :${item}`;
            newstr += !(indx > 0) ? ' ' + 'WHERE'+' ' + y.replace(`:${item}`, `'${param[item]}'`) : ' '+`AND`+' '+`${y.replace(`:${item}`, `'${param[item]}'`)}`;
        })
        this.sql += newstr;
        return this;
    }

    andWhere(){}

    getSql(){
        return this.sql + ';';
    }
}
