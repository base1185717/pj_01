import { StoreLayerI } from "../save.layer.base.interface"
import { UserCommandEntity } from "../../models/user.command.entity";

export enum typeCommandChainEnum {
    AddCar = 'AddCarChain'
}

class UserCommandRegistry<P extends UserCommandEntity> implements StoreLayerI <P> {

    constructor(private entity: {new (): P}){}

    private user_commands:Map<number, P> = new Map

 
    get(id):P|void{
        if(this.user_commands.has(id)) return  this.user_commands.get(id);
    }

    getAll(){
        return[ ... this.user_commands.entries() ];
    }
    
    set(entity:P){
        const id = entity.userId;
        this.user_commands.set(id, entity);
    }

    delete(){

    }

    update(){

    }

    edit(entity: P) {
        
    }
}

const userCommandRegistryObject = new UserCommandRegistry(UserCommandEntity);

export {userCommandRegistryObject,UserCommandRegistry }